\documentclass{article}
\usepackage[margin=1.25in]{geometry}
\usepackage{amsmath,amsfonts,amsthm,amssymb}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{footnote}
\usepackage{color,xcolor}
\usepackage{authblk}

\newcommand{\vct}{\boldsymbol }
\newcommand{\mat}{\mathbf}
\newcommand{\rnd}{\mathsf}
\newcommand{\ud}{\mathrm d}
\newcommand{\nml}{\mathcal{N}}
\newcommand{\loss}{\mathcal{L}}
\newcommand{\hinge}{\mathcal{R}}
\newcommand{\kl}{\text{KL}}
\newcommand{\cov}{\text{cov}}
\newcommand{\dir}{\text{Dir}}
\newcommand{\mult}{\text{Mult}}
\newcommand{\err}{\text{err}}
\newcommand{\sgn}{\text{sgn}}
\newcommand{\argmin}{\text{argmin}}
\newcommand{\argmax}{\text{argmax}}
\newcommand{\diag}{\mat{diag}}
\renewcommand{\mod}{\text{ mod }}
\newcommand{\median}{\text{med}}

\newtheorem{thm}{Theorem}
\newtheorem{lem}{Lemma}
\newtheorem{cor}{Corollary}
\newtheorem{prop}{Proposition}
\newtheorem{asmp}{Assumption}
\newtheorem{defn}{Definition}

\newcommand{\yining}[1]{{\color{red} [Yining: #1]}}

\title{Fast Robust Tensor Power Method via Count Sketch}
\author{Yining Wang }
\author{Hsiao-Yu Tung}
\author{Alex Smola}
\affil{Carnegie Mellon University}

\begin{document}

\maketitle

\section{Introduction}

Suppose we have a third order symmetric tensor $\mat T\in\mathbb R^{n\times n\times n}$ that has the following low rank decomposition:
\begin{equation}
\mat T = \sum_{i=1}^r{\lambda_i\vct v_i\otimes\vct v_i\otimes\vct v_i}=\sum_{i=1}^r{\lambda_i\vct v_i^{\otimes 3}},
\label{eq_tensor_eigen_decomp}
\end{equation}
where $\{\vct v_i\}_{i=1}^r\subseteq R^n$ is an orthonormal basis and $\{\lambda_i\}_{i=1}^r$ is a set of eigenvalues.
Without loss of generality, assume $\lambda_1\geq\cdots\geq\lambda_r > 0$.
Given a (possibly noisy) low-rank tensor $\mat T$ and its rank $r$, we want to find its eigen decomposition as in Eq. (\ref{eq_tensor_eigen_decomp}).

\subsection{Robust tensor power method}

Robust Tensor power method \cite{tensor-power-method} is a popular algorithm that recovers the eigen decomposition of a low-rank tensor $\mat T$.
Furthermore, it is robust to symmetric additive noise.
The algorithm is shown in Algorithm \ref{alg_tensor_power}.
\begin{algorithm}
\caption{Robust tensor power method, \cite{tensor-power-method}}
\begin{algorithmic}[1]
\State \textbf{Input}: noisy symmetric tensor $\widetilde{\mat T} = \mat T+\mat E\in\mathbb R^{n\times n\times n}$, number of iterations $L$ and $T$.
\For{$\tau=1$ to $L$}
	\State Draw $\vct u_0^{(\tau)}$ uniformly at random from the unit sphere $\mathcal S^n$.
	\For{$t=1$ to $T$}
		\State Compute power iteration update:
		\begin{equation}
		\vct u_t^{(\tau)} = \frac{\widetilde{\mat T}(\mat I, \vct u_{t-1}^{(\tau)}, \vct u_{t-1}^{(\tau)})}{\|\widetilde{\mat T}(\mat I, \vct u_{t-1}^{(\tau)}, \vct u_{t-1}^{(\tau)})\|}.
		\label{eq_power_iteration}
		\end{equation}
	\EndFor 
\EndFor
\State Let $\vct\tau^* = \argmax_{\tau=1}^L{\widetilde{\mat T}(\vct u_T^{(\tau)}, \vct u_T^{(\tau)}, \vct u_T^{(\tau)})}$.
\State Do $T$ power updates as in Eq. (\ref{eq_power_iteration}) starting from $\vct u_T^{(\tau^*)}$ to obtain $\hat{\vct u}$,
and set $\hat{\lambda} = \widetilde{\mat T}(\hat{\vct u}, \hat{\vct u}, \hat{\vct u})$.
\State \textbf{Output}: the estimated eigenvalue/eigenvector pair $(\hat\lambda, \hat{\vct u})$ and the deflated tensor $\widetilde{\mat T}-\hat\lambda\hat{\vct u}^{\otimes 3}$.
\end{algorithmic}
\label{alg_tensor_power}
\end{algorithm}

To obtain the complete orthogonal decomposition of a rank-$r$ tensor $\mat T$ one needs to run Algorithm \ref{alg_tensor_power}
for $r$ iterations, each time with the deflated tensor as input.
The total time complexity required is $O(rn^3LT)$.
Analysis shows that to obtain good approximation with high probability $L$ should be set as a near linear function of $r$
\yining{I think $L$ depends on $r$ rather than $n$, but needs further investigation. The tensor power method paper does not consider the $n>k$ setting for noisy inputs.}
and $T$ should be set as $\Omega(\log n)$ \cite{tensor-power-method}.
Therefore, the time complexity for decomposing a noisy rank-$r$ tensor using Algorithm \ref{alg_tensor_power} is approximately $O(r^2n^3\log n)$,
which is prohibitively high for large-scale applications.

\subsection{Matrix compression via count sketch}
Consider a matrix $\mat M\in\mathbb R^{n\times n}$ and a hash length $b$.
Suppose we have hash functions $h_1,h_2:[n]\to[b]$ chosen independently at random from a 3-wise independent hash family.
Let $\sigma_1,\sigma_2:[n]\to\{+1,-1\}$ be independent Radamacher random variables.
A \emph{count sketch} \cite{count-sketch} of the matrix $\mat M$ is a hash vector $s_M:[n]\to\mathbb R$ defined as
\footnote{To simplify notations, we will use $\vct s_{\mat M}=[s_{\mat M}(1),\cdots,s_{\mat M}(b)]\in\mathbb R^b$ to denote the vector version of $s_{\mat M}(\cdot)$.}
\begin{equation}
s_{\mat M}(t) := \sum_{h_1(i)+h_2(j)\mod b=t}{\mat M_{ij}\sigma_1(i)\sigma_2(j)},\quad\forall t=0,1,\cdots,b-1.
\label{eq_count_sketch}
\end{equation}
The corresponding recovery rule is
\begin{equation}
\widehat{\mat M_{ij}} = \sigma_1(i)\sigma_2(j)s_{\mat M}(h_1(i)+h_2(j)\mod b).
\label{eq_count_sketch_recovery}
\end{equation}
It can be shown that the recovery rule in Eq. (\ref{eq_count_sketch_recovery}) is unbiased,
that is, $\mathbb E_{h_1,h_2,\sigma_1,\sigma_2}[\widehat{\mat M_{ij}}] = \mat M_{ij}$.
The count sketch technique together with Fast Fourier Transformation (FFT) has been successfully applied
in the sparse matrix multiplication setting to obtain fast matrix multiplication algorithms \cite{compressed-matrix-multiplication}.

\subsection{Notations}

For two complex vectors $\vct a,\vct b\in\mathbb C^d$, we use $\vct a*\vct b\in\mathbb C^d$ to denote the convolution between $\vct a$ and $\vct b$.
Namely, $\vct a*\vct b\in\mathbb C^d$ and
\begin{equation}
[\vct a*\vct b]_n = \sum_{i=0}^{d-1}{\vct a_i\vct b_{(n-i)\mod d}}.
\end{equation}
We use $\vct a\circ\vct b$ to denote the entrywise product of $\vct a$ and $\vct b$:
\begin{equation}
\vct a\circ\vct b = (\vct a_0\vct b_0, \vct a_1\vct b_1,\cdots,\vct a_{d-1}\vct b_{d-1})\in\mathbb C^d.
\end{equation}
The $\langle\vct a,\vct b\rangle$ denotes the convex inner product between $\vct a$ and $\vct b$, that is, 
\begin{equation}
\langle\vct a,\vct b\rangle = \sum_{i=1}^n{\vct a_i\overline{\vct b_i}} \in\mathbb C.
\end{equation}

We use $\mathcal F(\cdot)$ to denote the discrete Fourier transformation (DFT) and $\mathcal F^{-1}(\cdot)$ to denote the inverse DFT.
The convolution theorem states that for any two convex valued vectors $\vct a,\vct b\in\mathbb R^d$.
$\vct a*\vct b = \mathcal F^{-1}(\mathcal F(\vct a)\circ\mathcal F(\vct b))$.
This allows $O(d\log d)$ computation of $\vct a*\vct b$ via fast Fourier transformation (FFT).

\section{Fast tensor power method via count sketch}

\subsection{Colliding hashes}

In our application all tensors are symmetric.
So it makes sense to design hash functions that deliberately collide symmetric entries.
More specifically, let $h:[n]\to b$ be a hash function randomly sampled from a 3-wise indepdent hash family 
and let $\sigma:[n]\to\mathbb C$ be $n$ i.i.d. Rademacher random variables in the complex domain,
that is,
\begin{equation}
\Pr\left[\sigma(k)=e^{i\frac{2\pi j}{m}}\right] = \frac{1}{m},\quad\forall k\in\{1,\cdots,n\}, j\in\{0,\cdots,m-1\}.
\end{equation}
Now define the count sketch of a symmetric tensor $\mat T\in\mathbb R^{n\times n\times n}$ as
(to simplify notations, let $h(i,j,k) := h(i)+h(j)+h(k)\mod b$)
\begin{equation}
s_{\mat T}(t) := \sum_{\substack{1\leq i\leq j\leq k\leq n,\\h(i,j,k)=t}}{\mat T_{i,j,k}\sigma(i)\sigma(j)\sigma(k)},\quad\forall t=0,1,\cdots,b-1.
\end{equation}
The corresponding recovery rule is
\begin{equation}
\widehat{\mat T}_{i,j,k} = \overline{\sigma(i)}\cdot\overline{\sigma(j)}\cdot\overline{\sigma(k)}\cdot s_{\mat T}(h(i,j,k)).
\end{equation}
It can be shown that if the period of the complex Rademacher random variables $m$ satisfies $m>3$ then
for any 3-tuple $(i,j,k)$, the recovered value satisfies $\mathbb E_{h,\sigma}[\widehat{\mat T}_{i,j,k}] = \mat T_{i',j',k'}$,
where $i'\leq j'\leq k'$ is a re-ordering of $(i,j,k)$.
This shows that our design completely ignores the order between tensor indices, which is what we desire.

If an input symmetric tensor $\mat T$ has $O(N)$ non-zero entries then its (colliding) count sketch $\vct s_{\mat T}$ can be computed
in $N/6$ steps, each step computing a 3-wise product $\mat T_{i,j,k}\sigma(i)\sigma(j)\sigma(k)$.
Furthermore, if $\mat T$ is exactly low rank and we know its low-rank decomposition then its count sketch can be computed efficiently.
To see this, note that the count sketch operator is linear (i.e., $\vct s_{\lambda\mat T+\mu\mat V} = \lambda\vct s_{\mat T} + \mu\vct s_{\mat V}$). 
Hence, it suffices to sketch a rank-1 tensor $\mat T=\vct u^{\otimes 3}$ for some $\vct u\in\mathbb R^n$.

The count sketch $\vct s_{\mat T}$ of $\mat T=\vct u^{\otimes 3}$ can be decomposed into three terms:
\begin{equation}
\vct s_{\mat T} = \frac{1}{6}\left(\vct a_{\mat T} + 3\vct b_{\mat T} + 2\vct c_{\mat T}\right),
\end{equation}
where
\begin{eqnarray*}
a_{\mat T}(t) &=& \sum_{h(i, j, k)=t}{\vct u_i\vct u_j\vct u_k\sigma(i)\sigma(j)\sigma(k)};\\
b_{\mat T}(t) &=& \sum_{h(i, i, j)=t}{\vct u_i^2\vct u_j\sigma(i)^2\sigma(j)};\\
c_{\mat T}(t) &=& \sum_{h(i, i, i)=t}{\vct u_i^3\sigma(i)^3}.
\end{eqnarray*}
It is clear that $\vct c_{\mat T}$ can be computed in $O(n)$ steps.
The first two terms ($\vct a_{\mat T}$ and $\vct b_{\mat T}$) can be computed in the following manner:
first compute the hash vector $\vct s_{\vct u}\in\mathbb C^b$ defined as
\begin{equation}
s_{\vct u}(t) = \sum_{h(i)\mod b=t}{\sigma(i)\vct u_i}.
\end{equation}
Subsequently, $\vct a_{\mat T} = \vct s_{\vct u}*\vct s_{\vct u}*\vct s_{\vct u}$
and $\vct b_{\mat T} = (\vct s_{\vct u}\circ\vct s_{\vct u})*\vct s_{\vct u}$.
As a result, 
\begin{equation}
\vct a_{\mat T} + 3\vct b_{\mat T} = \mathcal F^{-1}\left(\mathcal F(\vct s_{\vct u})\circ\mathcal F(\vct s_{\vct u})\circ\mathcal F(\vct s_{\vct u})
+ 3\mathcal F(\vct s_{\vct u}\circ\vct s_{\vct u})\circ\mathcal F(\vct s_{\vct u})\right).
\end{equation}
Hence, $\vct s_{\mat T}$ can be computed in $O(n+b\log b)$ time.

Finally, we remark that our colliding hash design achieves a $6\times$ speed-up for sparse inputs
and $1.5\times$ speed-up for exact low-rank inputs.
For the latter case, note that FFT is the computational bottleneck and we require 2 instead of 3 FFT computations
(the inverse FFT only needs to be computed once after we stack all low rank representations).

\subsection{Fast computation of tensor product}

The bottleneck of computation in Algorithm \ref{alg_tensor_power} is the computation of the two power updates
$\mat T(\mat I,\vct u,\vct u)$ and $\mat T(\vct u, \vct u, \vct u)$.
A naive implementation requires $O(n^3)$ float operations.
In this section, we show how to speed up the computation of the above tensor products.
More specifically, given the count sketch $\vct s_{\mat T}\in\mathbb C^b$ one can approximately compute both $\mat T(\mat I,\vct u,\vct u)$ and $\mat T(\vct u,\vct u, \vct u)$
in $O(b\log b+n)$ steps.

First consider the computation of $\mat T(\vct u,\vct u,\vct u)$.
Define $\vct s_{\vct u}\in\mathbb C^d$ as
\begin{equation}
\bar s_{\vct u}(t) := \sum_{h(i)\mod b=t}{\overline{\sigma(i)}\cdot \vct u_i},\quad\forall t=0,1,\cdots,b-1.
\end{equation}
Then the tensor product $\mat T(\vct u,\vct u,\vct u)$ can be approximately computed as
\begin{eqnarray}
\mat T(\vct u,\vct u,\vct u) 
&=& \sum_{i,j,k=1}^n{\mat T_{i,j,k}\vct u_i\vct u_j\vct u_k}\nonumber\\
&\approx& \sum_{i,j,k=1}^n{s_{\mat T}(h(i,j,k))\overline{\sigma(i)}\vct u_i\overline{\sigma(j)}\vct u_j\overline{\sigma(k)}\vct u_k}\\
&=& \sum_{i,j,k=1}^b{s_{\mat T}((i+j+k)\mod b)\bar s_{\mat u}(i)\bar s_{\mat u}(j)\bar s_{\mat u}(k)}\\
&=& \text{sum}\left(\vct s_{\mat T}*\bar{\vct s}_{\vct u}*\bar{\vct s}_{\vct u}*\bar{\vct s}_{\vct u}\right)\\
&=& \text{sum}\left(\mathcal F^{-1}(\mathcal F({\vct s}_{\mat T})\circ\mathcal F(\bar{\vct s}_{\vct u})\circ\mathcal F(\bar{\vct s}_{\vct u})\circ\mathcal F(\bar{\vct s}_{\vct u}))\right)\nonumber\\
&=& \langle\mathcal F({\vct s}_{\mat T}), \overline{\mathcal F(\bar{\vct s}_{\vct u})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct u})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct u})}\rangle.
\label{eq_tuuu_fast}
\end{eqnarray}
The third equation in Eq. (\ref{eq_tuuu_fast}) is due to the Poisson summation formula.
Note that one can compute $\vct s_{\vct u}$ in $O(n)$ steps;
the entrywise product and convex inner product can be computed in $O(b)$ steps;
the Fourier and inverse Fourier transformation can be computed in $O(b\log b)$ steps via FFT.
Therefore, $\mat T(\vct u,\vct u,\vct u)$ can be approximately computed in $O(n+b\log b)$ steps by Eq. (\ref{eq_tuuu_fast}).

Next we consider the computation of $\mat T(\mat I,\vct u,\vct u)$.
Let $\vct e_i=(0,\cdots,0,1,0,\cdots,0)\in\mathbb R^n$ denote the $i$th indicator vector.
Since $\mat T$ is symmetric, $\mat T(\mat I,\vct u,\vct u) = \mat T(\vct u,\vct u,\mat I)$ which can be further approximated by
\begin{eqnarray}
\left[\mat T(\vct u, \vct u, \mat I)\right]_i
&\approx& \langle\mathcal F(\vct s_{\mat T}), \overline{\mathcal F(\bar{\vct s}_{\vct u})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct u})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct e_i})}\rangle\nonumber\\
&=& \langle\mathcal F(\vct s_{\mat T})\circ\mathcal F(\bar{\vct s}_{\vct u})\circ\mathcal F(\bar{\vct s}_{\vct u}), \overline{\mathcal F(\bar{\vct s}_{\vct e_i})}\rangle\nonumber\\
&=& \langle\overline{\mathcal F({\vct s}_{\mat T})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct u})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct u})}, \mathcal F(\bar{\vct s}_{\vct e_i})\rangle\nonumber\\
&=& \left\langle\mathcal F^{-1}\left( \overline{\mathcal F({\vct s}_{\mat T})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct u})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct u})}\right), \bar{\vct s}_{\vct e_i}\right\rangle.
\label{eq_tiuu_fast}
\end{eqnarray}
The third equation above is due to the fact that $\mat T(\vct u,\vct u,\mat I)$ is a real vector.
Note that the first term in the inner product in Eq. (\ref{eq_tiuu_fast}) does not depend on $i$
and can be computed in $O(n+b\log b)$ steps.
For the second term, $\vct e_i$ has only one non-zero entry and so is $\bar{\vct s}_{\vct e_i}$.
Therefore, once the length-$b$ vector $\mathcal F^{-1}(\overline{\mathcal F(\vct s_{\mat T})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct u})}\circ\overline{\mathcal F(\bar{\vct s}_{\vct u})})$
independent of $i$ is computed we can (approximately) recover the vector $\mat T(\mat I,\vct u,\vct u)$ in $O(n)$ time
by reading off $n$ terms.
The total time complexity for computing $\mat T(\mat I,\vct u,\vct u)$ is still $O(n+b\log b)$.

\subsection{The complete algorithm}

The pseudocode of the complete fast robust tensor power method is presented in Algorithm \ref{alg_fast_tensor_power}.
Note that we boost the approximation accuracy by using $B$ independent hashes and taking the median of the outcomes.

\begin{algorithm}[htbp]
\caption{Fast tensor power method}
\begin{algorithmic}[1]
\State \textbf{Input}: noisy symmetric tensor $\widetilde{\mat T}=\mat T+\mat E\in\mathbb R^{n\times n\times n}$,
number of iterations $L$ and $T$,
hash length $b$ and number of hashes $B$.
\State \textbf{Initialize}: $B$ independent index hash functions $h^{(1)},\cdots, h^{(B)}$ and $\sigma^{(1)},\cdots,\sigma^{(B)}$.
\State For $m=1,\cdots, B$ compute the count sketch of $\widetilde{\mat T}$, $\vct s_{\widetilde{\mat T}}^{(m)}\in\mathbb R^b$.
\For{$\tau=1$ to $L$}
	\State Draw $\vct u_0^{(\tau)}$ uniformly at random from the unit sphere $\mathcal S^{n-1}$.
	\For{$t=1$ to $T$}
		\State For each $m=1,\cdots, B$ compute $\vct v^{(b)} \approx \widetilde{\mat T}(\mat I,\vct u_{t-1}^{(\tau)}, \vct u_{t-1}^{(\tau)})$
		using the $m$th sketches via Eq. (\ref{eq_tiuu_fast}).
		\State Take the median estimate: $\bar{\vct v}_i = \median(\Re(\vct v^{(1)}_i), \Re(\vct v^{(2)}_i), \cdots, \Re(\vct v^{(B)}_i))$.
		\State Update: $\vct u_t^{(\tau)} = \bar{\vct v} / \|\bar{\vct v}\|$.
	\EndFor
\EndFor
\State For each $\tau\in[L]$ and $m\in[B]$ compute $\lambda_{\tau}^{(m)} \approx \widetilde{\mat T}(\vct u_T^{(\tau)},\vct u_T^{(\tau)}, \vct u_T^{(\tau)})$
using the $m$th sketches via Eq. (\ref{eq_tuuu_fast}). 
Let $\tau^* = \argmax_{\tau\in[L]}{\median(\Re(\lambda_{\tau}^{(1)}),\cdots, \Re(\lambda_{\tau}^{(B)}))}$.
\State Do $T$ power updates starting from $\vct u_T^{(\tau^*)}$ to obtain $\hat{\vct u}$; compute $\hat\lambda = \widetilde{\mat T}(\hat{\vct u},\hat{\vct u},\hat{\vct u})$.
\State For each $m\in[B]$ compute the count sketch $\vct s_{\Delta\mat T}^{(m)}$ for the low-rank matrix $\Delta\mat T = \hat{\lambda}\hat{\vct u}^{\otimes 3}$.
\State \textbf{Output}: the eigenvalue/eigenvector pair $(\hat\lambda,\hat{\vct u})$ and count sketches of the deflated tensor $\widetilde{\mat T}-\Delta\mat T$.
\end{algorithmic}
\label{alg_fast_tensor_power}
\end{algorithm}

\bibliographystyle{alpha}
\bibliography{fftspec}

\end{document}
